#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <regex.h> 

static const char *dirPath = "/home/wildan";
char prefix[20] = "/module_";
regex_t regex;

int isDir(const char* fileName)
{
    struct stat path;

    stat(fileName, &path);

    return S_ISREG(path.st_mode);
}

void modularisasi(const char *basePath) {
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    char filename[100];
    // readdir
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {   
            strcpy(filename, dp->d_name);
            char path[300];
            sprintf(path, "%s/%s", basePath, filename);
            int dir = 0;
            dir = isDir(path);

            // if folder -> modularisasi(folder) [rekursif]
            if (dir == 0)
            {
                char childPath[200];
                sprintf(childPath, "%s/%s", basePath, filename);
                printf("modul path : %s", childPath);
                modularisasi(childPath);
            }

            // if file -> dibagi jadi file-file kecil, hapus file asli
            else 
            {   
                char split[1000];
                sprintf(split, "split -b 1024 -a 3 -d %s %s.", path, path);
                printf("split command : %s\n", split);
                system(split);
                char rm[1000];
                sprintf(rm, "rm %s", path);
                printf("rm command : %s\n", rm);
                system(rm);
            }
        }
    }
    closedir(dir); 
}

void remodularisasi(char *basePath) {
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    char filename[100];
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {   
            strcpy(filename, dp->d_name);
            char path[300];
            sprintf(path, "%s/%s", basePath, filename); 
            printf("path: %s\n", path);
            int dir = 0;
            dir = isDir(path);

            // if folder -> remodularisasi(folder) [rekursif]
            if (dir == 0)
            {
                char childPath[200];
                sprintf(childPath, "%s/%s", basePath, filename);
                remodularisasi(path);
            }

            // if file -> dijadikan satu, hapus file asli
            else 
            {   
                char mergePath[300];
                sprintf(mergePath, "%s/%s", basePath, filename); 
                char *dot = strrchr(mergePath, '.');  
                if (dot != NULL) {
                    *dot = '\0'; 
                }
                printf("merge path: %s\n", mergePath);
                char oldPath[400] = {0};
                if (strcmp(oldPath, mergePath) != 0){
                    strcpy(oldPath, mergePath);
                    char merge[1000];
                    sprintf(merge, "cat %s.* > %s", oldPath, oldPath);
                    printf("merge command : %s\n", merge);
                    system (merge);
                    char rm[1000];
                    sprintf(rm, "rm %s.*", oldPath);
                    printf("rm command : %s\n", rm);
                    system(rm);
                }
            }
        }
    }
    closedir(dir);                        
}

void writeLog(char *func, const char *prev, const char *new)
{
    time_t curTime;
    struct tm *time_info;
    time(&curTime);
    time_info = localtime(&curTime);
    int year = time_info->tm_year + 1900;
    int month = time_info->tm_mon + 1;
    int day = time_info->tm_mday;
    int hour = time_info->tm_hour;
    int min = time_info->tm_min;
    int sec = time_info->tm_sec;
    FILE *logFile;
    logFile = fopen("/home/wildan/fs_module.log", "a");
    char level[10];
    char use[3] = "";
    if (strcmp(new, "") != 0)
        strcpy(use, "::");
    if (strcmp(func, "RMDIR") == 0 || strcmp(func, "UNLINK") == 0)
        strcpy(level, "FLAG");
    else
        strcpy(level, "REPORT");
    fprintf(logFile, "%s::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s%s\n", level, day, month, year, hour, min, sec, func, prev, use, new);
    fclose(logFile);
    return;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    printf("path getattr %s\n", path);
    char *mode = strstr(path, prefix);
    if (mode != NULL)
    {
        mode += strlen(prefix);
        printf("3 %s\n", mode);
        char *temp = strchr(mode, '/');
        if (temp)
        {
            printf(">3 %s\n", temp);
            modularisasi(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
        sprintf(newFullPath, "%s%s", dirPath, path);
    if (lstat(newFullPath, stbuf) == -1)
        return -errno;
    return 0;
}

static int xmp_access(const char *path, int mask)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("access %s\n", fpath);
    int result = access(fpath, mask);
    if (result == -1)
        return -errno;

    writeLog("ACCESS", path, "");
    return 0;
}

static int xmp_readlink(const char *path, char *data, size_t data_size){
	int res;

    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
	res = readlink(fpath, data, data_size);
	if (res == -1)
		return -errno;

	writeLog("READLINK", path, "");
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    printf("path readdir %s\n", path);
    char *mode = strstr(path, prefix);
    if (mode != NULL)
    {
        mode += strlen(prefix);
        printf("rd mode %s\n", mode);
        char *temp = strchr(mode, '/');
        if (temp)
        {
            printf("rd temp %s\n", temp);
            modularisasi(temp);
            printf("rd path %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
        sprintf(newFullPath, "%s%s", dirPath, path);
    
    DIR *folder;
    struct dirent *de;
    (void)offset;
    (void)fi;
    folder = opendir(newFullPath);
    if (folder == NULL)
        return -errno;
    while ((de = readdir(folder)) != NULL)
    {
        if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))
            continue;
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (filler(buf, de->d_name, &st, 0))
            break;
    }
    closedir(folder);
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
    int res;

    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path );
	res = mknod(fpath, mode, rdev);
	if (res == -1)
		return -errno;

	writeLog("MKNOD", path, "");
    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    printf("path mkdir %s\n", path);
    // mkdir default
    int res;
    res = mkdir(path, mode);
    if (res == -1)
        return -errno;

    // cek modular
    int return_value;
    return_value = regcomp(&regex, prefix, 0);
    return_value = regexec(&regex, path, 0, NULL, 0);
    if(return_value == 0) {
        modularisasi(path);
        printf("MKDIR %s adalah directory modular\n", path);
    }

    // log
    writeLog("MKDIR", path, "");

    return 0;
}

static int xmp_symlink(const char *path, const char *to)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("symlink %s\n", fpath);
    int result = symlink(fpath, to);
    if (result == -1)
        return -errno;

    writeLog("SYMLINK", path, "");
    return 0;
}

static int xmp_unlink(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("unlink %s\n", fpath);
    
    int result = unlink(fpath);
    if (result == -1)
        return -errno;

    writeLog("UNLINK", path, "");
    return 0;
}

static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("rmdir %s\n", fpath);
    
    int result = rmdir(fpath);
    if (result == -1)
        return -errno;

    writeLog("RMDIR", path, "");
    return 0;
}

static int xmp_rename(const char *old, const char *new)
{
    // rename default
    int res;
    char oldPath[1000], newPath[1000];

    sprintf(oldPath, "%s%s", dirPath, old);
    sprintf(newPath, "%s%s", dirPath, new);
    res = rename(oldPath, newPath);
    if (res == -1)
        return -errno;
    
    // cek modular
    int return_value;
    return_value = regcomp(&regex, prefix, 0);
    return_value = regexec(&regex, newPath, 0, NULL, 0);
    if(return_value == 0) {
        printf("RENAME %s adalah directory modular\n", newPath);
        modularisasi(newPath);
    }
    else{
        remodularisasi(newPath);
    }


    writeLog("RENAME", old, new);

    return 0;
}

static int xmp_link(const char *path, const char *to)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("link %s\n", fpath);
    int result = link(fpath, to);
    if (result == -1)
        return -errno;
    
    writeLog("OPEN", path, "");
    return 0;
}

static int xmp_chmod(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("chmod %s\n", fpath);
    
    int result = chmod(fpath, mode);
    if (result == -1)
        return -errno;
    
    writeLog("CHMOD", path, "");
    return 0;
}

static int xmp_chown(const char *path, uid_t uid, gid_t gid)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("chown %s\n", fpath);
   
    int result = lchown(fpath, uid, gid);
    if (result == -1)
        return -errno;
    writeLog("CHOWN", path, "");
    return 0;
}

static int xmp_truncate(const char *path, off_t size)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("truncate %s\n", fpath);
    
    int result = truncate(fpath, size);
    if (result == -1)
        return -errno;
    
    writeLog("TRUNCATE", path, "");
    return 0;
}

static int xmp_utimens(const char *path, const struct timespec ts[2])
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("utimens %s\n", fpath);
    int result = utimensat(0, fpath, ts, AT_SYMLINK_NOFOLLOW);
    if (result == -1)
        return -errno;
    
    writeLog("UTIMENS", path, "");
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    int res = 0;
    char fpath[1000];

    strcpy(fpath, dirPath);
    strncat(fpath, path, sizeof(fpath) - strlen(fpath) - 1);

    res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;

    fi->fh = res;

    writeLog("OPEN", path, "");
    close(res);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    printf("path read %s\n", path);
    // char *mode = strstr(path, prefix);
    // if (mode != NULL)
    // {
    //     mode += strlen(prefix);
    //     printf("3 %s\n", mode);
    //     char *temp = strchr(mode, '/');
    //     if (temp)
    //     {
    //         temp += 1;
    //         printf(">3 %s\n", temp);
    //         // getNormalFile(temp);
    //         printf("<3 %s\n", path);
    //     }
    // }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
        sprintf(newFullPath, "%s%s", dirPath, path);
    int fd;
    int res;
    (void)fi;
    fd = open(newFullPath, O_RDONLY);
    if (fd == -1)
        return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    writeLog("READ", path, "");

    close(fd);
    return res;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("write %s\n", fpath);
    
    int fd;
    int res;

    (void)fi;
    fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    writeLog("WRITE", path, "");
    close(fd);
    return res;
}

static int xmp_statfs(const char *path, struct statvfs *stbuf)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("statfs %s\n", fpath);
    int result = statvfs(fpath, stbuf);
    if (result == -1)
        return -errno;
    
    writeLog("STATFS", path, "");
    return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    printf("path create %s\n", path);
    char *modee = strstr(path, prefix);
    if (modee != NULL)
    {
        modee += strlen(prefix);
        printf("3 %s\n", modee);
        char *temp = strchr(modee, '/');
        if (temp)
        {
            temp += 1;
            printf(">3 %s\n", temp);
            // getNormalFile(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
    {
        sprintf(newFullPath, "%s%s", dirPath, path);
    }

    int res;
    res = creat(newFullPath, mode);
    if (res == -1)
        return -errno;

    writeLog("CREATE", path, "");

    return 0;
}

static int xmp_release(const char *path, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    // printf("release %s\n", fpath);
    writeLog("RELEASE", path, "");
    close(fi->fh);
    return 0;
}

static int xmp_fsync(const char *path, int isdatasync, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("fsync %s\n", fpath);
    int result;
    if (isdatasync)
        result = fdatasync(fi->fh);
    else
        result = fsync(fi->fh);
    if (result == -1)
        return -errno;
    
    writeLog("FSYNC", path, "");
    return 0;
}

static int xmp_setxattr(const char *path, const char *name, const char *value, size_t size, int flags)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("setxattr %s\n", fpath);
    int result = setxattr(fpath, name, value, size, flags);
    if (result == -1)
        return -errno;
    writeLog("SETXATTR", path, "");
    return 0;
}

static int xmp_getxattr(const char *path, const char *name, char *value, size_t size)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("getxattr %s\n", fpath);
    int result = lgetxattr(fpath, name, value, size);
    if (result == -1)
        return -errno;
    writeLog("GETXATTR", path, "");
    return result;
}

static int xmp_listxattr(const char *path, char *list, size_t size)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("listxattr %s\n", fpath);
    int result = llistxattr(fpath, list, size);
    if (result == -1)
        return -errno;
    writeLog("LISTXATTR", path, ""); 
    return result;
}

static int xmp_removexattr(const char *path, const char *name)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    printf("removexattr %s\n", fpath);
    int result = lremovexattr(fpath, name);
    if (result == -1)
        return -errno;
    writeLog("REMOVEXATTR", path, "");
    return 0;
}

static  struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .access = xmp_access,
    .readlink = xmp_readlink,
    .readdir = xmp_readdir,
    .mknod = xmp_mknod,
    .mkdir = xmp_mkdir,
    .symlink = xmp_symlink,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .link = xmp_link,
    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .truncate = xmp_truncate,
    .utimens = xmp_utimens,
    .open = xmp_open,
    .read = xmp_read,
    .write = xmp_write,
    .statfs = xmp_statfs,
    .create = xmp_create,
    .release = xmp_release,
    .fsync = xmp_fsync,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
    .removexattr = xmp_removexattr,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}


// gcc -Wall `pkg-config fuse --cflags` modular.c -o modular `pkg-config fuse --libs`
// ./modular -f folderModular
