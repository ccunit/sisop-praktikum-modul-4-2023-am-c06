# sisop-praktikum-modul-4-2023-AM-c06

| NRP | Nama |
|:---:|:----:|
| 5025211001 | Andika Laksana Putra |
| 5025211150 | Jawahirul Wildan |
| 5025211180 | Mohammad Kamal |

1. Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

- a. Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

```kaggle datasets download -d bryanb/fifa-player-stats-database```

- Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

- b. Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

- c. Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan. Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

- d. Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

- e. Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

## Penyelesaian

### soal 1.a

untuk dapat memperoleh data dari kaggle dataset, kita dapat menggunakan kode yang disediakan didalam soal. Namun, command `kaggle` didapatkan dari pip (pyton) sehingga pertama kali kita perlu menginstall python kedalam OS Ubuntu (apabila sebelumnya belum punya) dan menginstall kaggle dengan command pip.

```sh
pip install kaggle
```

setelah kita install kaggle, barulah kita bisa menggunakan command kaggle seperti yang ada pada soal. Pada penyelesaian soal ini, kami menggunakan process dalam melakukan download dan unzip didalam file storage.c sehingga apabila dieksekusi kita akan dapatkan 5 dataset dari link kaggle yang kita download. implementasi kodenya bisa dilihat dibawah ini :

```c
pid_t pid1, pid2;
    int status;

    pid1 = fork();
    if(pid1 == 0) {
        execlp("kaggle", "kaggle", "datasets", "download", "-d","bryanb/fifa-player-stats-database", NULL);
    }

    wait(&status);
    
    pid2 = fork();
    if (pid2 == 0) {
        execlp("unzip", "unzip", "-q", "fifa-player-stats-database.zip", NULL);
    }
```
### soal 1.b

Pada soal ini, kita diminta untuk melakukan oleh data sesuai dengan keinginan soal yaitu hanya menampilkan pemain-pemain FIFA23 yang memiliki umur dibawah 25, potensi diatas 85, dan tidak bermain di club "Manchester City". Kami memanfaatkan fungsi AWK untuk melakukan oleh data dan hanya menampilkan beberapa data sesuai kolom yang penting, implementasi kodenya dapat dilihat dibawah ini :

```c
    else {
        wait(&status);
            system("echo \"Potential \t Age \t Name \t\t\t Club \t\t\t\t Nationality \t\t Photo \" ");
            printf("------------------------------------------------------------------------------------------------------------------------\n");
            execlp("bash", "bash", "-c", "awk -F ',' '{if($3<=24 && $8>=86 && $9!=\"Manchester City\") {print $8 \"\t\t\" $3 \"\t\" $2 \"\t\t\" $9 \"\t\t\" $5 \"\t\t\" $4} }' FIFA23_official_data.csv", NULL);
    }
```

sehingga didapatkan hasil seperti dibawah ini :

![image](/uploads/d25e8694b3854c773b5b5334335d21e3/image.png)

### soal 1.c

Pada soal ini, kita diminta untuk membuat docker image dan menjalankan docker container dari docker image yang sudah kita build sebelumnya. Pertama kita perlu membuat Dockerfile seperti dibawah ini :

```Dockerfile
# Base image
FROM python:3.9

# Instalasi GCC
RUN apt-get update && apt-get install -y gcc

# Instalasi Kaggle CLI
RUN pip install kaggle

# Menyalin file requirements.txt ke dalam container
# COPY requirements.txt /app/

# Menyalin API Kaggle
COPY kaggle.json /root/.kaggle/kaggle.json

RUN chmod 600 /root/.kaggle/kaggle.json

# Menyalin file C ke dalam container
COPY storage.c /app/

# Mengatur working directory
WORKDIR /app

# Instalasi dependencies pip
# RUN pip install -r requirements.txt

# Kompilasi file C
RUN gcc -o storage storage.c

CMD ["sh", "-c", "while true; do sleep 7200; done"]
```

Kami memanfaatkan base image dari python:3.9 supaya lebih mudah untuk melakukan instalasi kaggle dengan command yang serupa dengan sebelumnya. selanjutnya kita lakukan `RUN apt-get update` supaya dapat mengupdate OS Ubuntu pada container yang kita gunakan. Selanjutnya, `apt-get install -y gcc` untuk menginstall compile gcc pada container tersebut. `RUN pip install kaggle` untuk menginstall kaggle kedalam container. Lalu kita perlu untuk menyalin API kaggle kedalam container supaya kita mendapatkan akses untuk mengunduh dataset dari kaggle dengan `COPY kaggle.json /root/.kaggle/kaggle.json` dan `RUN chmod 600 /root/.kaggle/kaggle.json`. Sisanya kita hanya perlu mengcopy file yang dibutuhkan yaitu storage.c lalu lakukan compile dengan command seperti biasa yaitu `RUN gcc -o storage storage.c`. Terakhir, kita jalankan `CMD ["sh", "-c", "while true; do sleep 7200; done"]` supaya container tidak exit apabila aplikasi dijalankan (command terakhir ini dimanfaatkan pada docker-compose karena sebelumnya selalu exit setelah dijalankan). 

Setelah itu, Dockerfile kita simpan satu directory dengan file-file yang kita butuhkan yaitu kaggle.json dan storage.c. jika sudah kita jalankan kode dibawah ini diterminal:

```sh
docker build -t puyul123/storage-app:latest .
```
kita akan melakukan build menjadi docker image dengan nama image puyul123/storage-app (penamaan ini dibutuhkan nantinya untuk upload ke docker hub). Selanjutnya kita perlu melakukan run docker image menjadi container dengan menjalankan kode dibawah ini:

```sh
docker run -it -d puyul123/storage-app:latest
```

setelah dijalankan, docker akan berjalan secara daemon dibelakang layar dan kita sudah dapat mengakses container tersebut dengan kode dibawah ini:

```sh
docker exec -it [nama container] /bin/bash
```

selanjutnya kita hanya perlu menjalankan perintah terminal sebelumnya didalam terminal directory docker container kita. hasilnya dapat dilihat seperti dibawah ini.

![image](/uploads/144ae06f3f182c21b3f916a20ca8883f/image.png)

### soal 1.d

penyelesaian pada soal ini cukup mudah, Pertama kita harus memiliki akun docker terlebih dahulu dan melakukan login di docker desktop (jika belum bisa melakukan sign up). Setelah itu kita masukkan perintah `docker login` kedalam terminal untuk melakukan autentikasi dengan docker desktop yang terhubung. Jika sudah terhubung, terakhir kita hanya perlu memasukkan perintah dibawah ini untuk dapat mengupload image kedalam docker hub.

```sh
docker push puyul123/storage-app:latest
```

### soal 1.e

Pada soal ini kita diminta untuk membuat docker compose supaya nanti kita dapat membuat 5 instance container dari masing-masing folder yaitu Barcelona dan Napoli. pertama kita perlu membuat file docker-compose.yml dengan isi sebagai berikut:

```yml
version: '3'
services:
  barcelona:
    build:
      context: ./Barcelona
    deploy:
      replicas: 5
  napoli:
    build:
      context: ./Napoli
    deploy:
      replicas: 5
```
secara singkat kita akan melakukan build pada isi dari masing-masing directory yaitu Barcelona dan Napoli. Selanjutnya, kita membuat directory Barcelona dan Napoli dengan masing-masing directory berisi Dockerfile, kaggle.json, dan storage.c dari sebelumnya sehingga nanti bisa dibuild dengan docker compose. Pastikan bahwa file docker-compose.yml dengan 2 directory sebelumnya 1 path. Terakhir kita hanya perlu memasukkan perintah kedalam terminal dibawah ini:

```sh
docker-compose up -d
```
maka didapatkan total 10 container (5 container Barcelona, dan 5 container Napoli). untuk mencobanya, kita hanya perlu masuk kedalam salah satu container lalu kita jalankan command seperti pada docker container sebelumnya.


3. Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

- a. Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 

- b. Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

- c. Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

- d. Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).

- e. Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
  - Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
  - Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 

- f. Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂

## Penyelesaian 

Soal ini meminta kita untuk melakukan mounting FUSE dengan melakukan beberapa operasi yang melibatkan manipulasi nama file dan directory.

### soal a 

Soal a menyuruh untuk membuat FUSE dengan melakukan mounting /etc.
  

	static const char *dirpath = "/Users/kamal/Downloads/inifolderetc/sisop";

  
	
	static int xmp_getattr(const char *path, struct stat *stbuf)
	
	{
	
	int res;
	
	char fpath[1000];
	
	  
	
	sprintf(fpath, "%s%s", dirpath, path);
	
	  
	
	res = lstat(fpath, stbuf);
	
	  
	
	if (res == -1)
	
	return -errno;
	
	  
	
	return 0;
	
	}

  
	
	static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
	
	{
	
	char fpath[1000];
	
	  
	
	if (strcmp(path, "/") == 0)
	
	{
	
	path = dirpath;
	
	sprintf(fpath, "%s", path);
	
	}
	
	else
	
	sprintf(fpath, "%s%s", dirpath, path);
	
	  
	
	int res = 0;
	
	  
	
	DIR *dp;
	
	struct dirent *de;
	
	(void)offset;
	
	(void)fi;
	
	  
	
	dp = opendir(fpath);
	
	  
	
	if (dp == NULL)
	
	return -errno;
	
	  
	
	while ((de = readdir(dp)) != NULL)
	
	{
	
	struct stat st;
	
	  
	
	memset(&st, 0, sizeof(st));
	
	  
	
	st.st_ino = de->d_ino;
	
	st.st_mode = de->d_type << 12;
	
	char name[100];
	
	strcpy(name, de->d_name);
	
	char cur_path[1500];
	
	sprintf(cur_path, "%s/%s", fpath, de->d_name);
	
	  
	
	res = (filler(buf, name, &st, 0));
	
	  
	
	if (res != 0)
	
	break;
	
	if(de->d_type == DT_REG) {
	
	if(is_encoded(name)) {
	
	encodeBase64File(cur_path);
	
	}
	
	else if(strlen(name) <= 4) {
	
	convert_to_binary(name);
	
	}
	
	else {
	
	modify_filename(name);
	
	}
	
	changePath(cur_path, name);
	
	}
	
	else {
	
	if(strlen(name) <= 4) {
	
	convert_to_binary(name);
	
	}
	
	else {
	
	modify_directoryname(name);
	
	}
	
	changePath(cur_path, name);
	
	xmp_readdir(cur_path, buf, filler, offset, fi);
	
	}
	
	}
	
	  
	
	closedir(dp);
	
	  
	
	return 0;
	
	}
	
	  
	
	static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
	
	{
	
	char fpath[1000];
	
	if (strcmp(path, "/") == 0) {
	
	path = dirpath;
	
	sprintf(fpath, "%s", path);
	
	} else
	
	sprintf(fpath, "%s%s", dirpath, path);
	
	  
	
	int res = 0;
	
	int fd = 0;
	
	  
	
	(void)fi;
	
	  
	
	fd = open(fpath, O_RDONLY);
	
	if (fd == -1)
	
	return -errno;
	
	  
	
	res = pread(fd, buf, size, offset);
	
	if (res == -1)
	
	res = -errno;
	
	  
	
	close(fd);
	
	  
	
	return res;
	
	}
	
	  
	
	static int xmp_open(const char *path, struct fuse_file_info *fi)
	
	{
	
	(void)fi;
	
	  
	
	if (strcmp(path, "/") == 0) {
	
	path = dirpath;
	
	} else {
	
	char fpath[1000];
	
	sprintf(fpath, "%s%s", dirpath, path);
	
	if (access(fpath, F_OK) == 0) {
	
	char input_password[100];
	
	printf("Masukkan password: ");
	
	fflush(stdout);
	
	fgets(input_password, sizeof(input_password), stdin);
	
	  
	
	size_t len = strlen(input_password);
	
	if (len > 0 && input_password[len - 1] == '\n')
	
	input_password[len - 1] = '\0';
	
	  
	
	while (strcmp(input_password, password) != 0) {
	
	printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
	
	fflush(stdout);
	
	fgets(input_password, sizeof(input_password), stdin);
	
	  
	
	len = strlen(input_password);
	
	if (len > 0 && input_password[len - 1] == '\n')
	
	input_password[len - 1] = '\0';
	
	}
	
	}
	
	}
	
	  
	
	return 0;
	
	}
	
	  
	
	static int xmp_mkdir(const char *path, mode_t mode)
	
	{
	
	char fpath[1000];
	
	sprintf(fpath, "%s%s", dirpath, path);
	
	  
	
	// Modify the directory name to all uppercase
	
	char modified_name[256];
	
	strcpy(modified_name, path);
	
	for (int i = 0; modified_name[i] != '\0'; i++) {
	
	modified_name[i] = toupper(modified_name[i]);
	
	}
	
	  
	
	// Create the directory with the modified name
	
	int res = mkdir(fpath, mode);
	
	if (res == -1) {
	
	return -errno;
	
	}
	
	  
	
	return 0;
	
	}
	
	  
	
	static int xmp_rename(const char *from, const char *to)
	
	{
	
	char fpath_from[1000];
	
	char fpath_to[1000];
	
	sprintf(fpath_from, "%s%s", dirpath, from);
	
	sprintf(fpath_to, "%s%s", dirpath, to);
	
	  
	
	// Modify the destination name to all uppercase
	
	char modified_name[256];
	
	strcpy(modified_name, to);
	
	for (int i = 0; modified_name[i] != '\0'; i++) {
	
	modified_name[i] = toupper(modified_name[i]);
	
	}
	
	  
	
	// Rename the file/directory with the modified name
	
	int res = rename(fpath_from, fpath_to);
	
	if (res == -1) {
	
	return -errno;
	
	}
	
	  
	
	return 0;
	
	}
	
	  
	
	static struct fuse_operations xmp_oper = {
	
	.getattr = xmp_getattr,
	
	.readdir = xmp_readdir,
	
	.read = xmp_read,
	
	.open = xmp_open,
	
	.mkdir = xmp_mkdir,
	
	.rename = xmp_rename,
	
	};
	
	  
	
	int main(int argc, char *argv[])
	
	{
	
	umask(0);
	
	  
	
	// Run the FUSE filesystem
	
	int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);
	
	  
	
	return fuse_stat;
	
	}


Kode-kode di atas merupakan mounting dan fungsi FUSE. Fungsi-fungsi termasuk : 

	.getattr = xmp_getattr,
	
	.readdir = xmp_readdir,
	
	.read = xmp_read,
	
	.open = xmp_open,
	
	.mkdir = xmp_mkdir,
	
	.rename = xmp_rename

### soal b
Soal b menyuruh untuk mengubah semua direktori dan file yang diawali L,U,T,H dengan base64 encoding. Dan encoding berlaku rekrusif. 

  
	
	static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	
	  
	
	void encodeBase64File(const char *filename) {
	
	FILE *file = fopen(filename, "rb");
	
	if (file == NULL) {
	
	printf("Failed to open file: %s\n", filename);
	
	return;
	
	}
	
	  
	
	// Get file size
	
	fseek(file, 0, SEEK_END);
	
	long file_size = ftell(file);
	
	fseek(file, 0, SEEK_SET);
	
	  
	
	// Allocate memory for file content
	
	uint8_t *file_content = (uint8_t *)malloc(file_size);
	
	if (file_content == NULL) {
	
	printf("Memory allocation failed\n");
	
	fclose(file);
	
	return;
	
	}
	
	  
	
	// Read file content
	
	size_t bytes_read = fread(file_content, 1, file_size, file);
	
	fclose(file);
	
	  
	
	if (bytes_read != file_size) {
	
	printf("Failed to read file: %s\n", filename);
	
	free(file_content);
	
	return;
	
	}
	
	  
	
	// Encode file content as Base64
	
	uint8_t input[3], output[4];
	
	size_t input_len = 0;
	
	size_t output_len = 0;
	
	size_t i = 0;
	
	file = fopen(filename, "wb");
	
	if (file == NULL) {
	
	printf("Failed to open file for writing: %s\n", filename);
	
	free(file_content);
	
	return;
	
	}
	
	  
	
	while (i < file_size) {
	
	input[input_len++] = file_content[i++];
	
	if (input_len == 3) {
	
	output[0] = base64_table[input[0] >> 2];
	
	output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
	
	output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
	
	output[3] = base64_table[input[2] & 0x3F];
	
	  
	
	fwrite(output, 1, 4, file);
	
	  
	
	input_len = 0;
	
	output_len += 4;
	
	}
	
	}
	
	  
	
	if (input_len > 0) {
	
	for (size_t j = input_len; j < 3; j++) {
	
	input[j] = 0;
	
	}
	
	  
	
	output[0] = base64_table[input[0] >> 2];
	
	output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
	
	output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
	
	output[3] = base64_table[input[2] & 0x3F];
	
	  
	
	fwrite(output, 1, input_len + 1, file);
	
	  
	
	for (size_t j = input_len + 1; j < 4; j++) {
	
	fputc('=', file);
	
	}
	
	  
	
	output_len += 4;
	
	}
	
	  
	
	fclose(file);
	
	free(file_content);
	
	  
	
	printf("File encoded successfully. file: %s\n", filename);
	
	}
	
	  
	
	bool is_encoded(const char *name) {
	
	char first_char = name[0];
	
	if (first_char == 'l' || first_char == 'L' ||
	
	first_char == 'u' || first_char == 'U' ||
	
	first_char == 't' || first_char == 'T' ||
	
	first_char == 'h' || first_char == 'H') {
	
	return true;
	
	}
	
	return false;
	
	}

Kode di atas merupakan fungsi yang mengencode ke base64. Pertama fungsi `encodeBase64File` read file.
Lalu mendapatkan file size mengunakan `fseek` lalu mengalokasikan memory untuk file content. Lalu read file content dengan `fread`. Lalu lakukan encoding. Setelah melakukan encoding, panggil `free` karena fungsi bersifat rekursif agar bisa dipanggil lagi. Lalu `is_encoded` untuk melakukan encoding hanya L,U,T,H.


### Soal c
Soal ini menyuruh untuk mengganti nama file dengan lowercase dan nama directory dengan uppercase. 
Lalu ganti nama directory dan file yang kurang dari atau sama dengan empat dengan binary. 

  
	
	void modify_filename(char *name)
	
	{
	
	for (int i = 0; name[i]; i++)
	
	{
	
	name[i] = tolower(name[i]);
	
	}
	
	}
	
	  
	
	void modify_directoryname(char *name)
	
	{
	
	for (int i = 0; name[i]; i++)
	
	{
	
	name[i] = toupper(name[i]);
	
	}
	
	}
	
	  
	
	void convert_to_binary(char *name)
	
	{
	
	int len = strlen(name);
	
	char *temp = (char *)malloc((8 * len + len) * sizeof(char)); // Allocate memory for modified string
	
	  
	
	int index = 0; // Index for the modified string
	
	  
	
	for (int i = 0; i < len; i++)
	
	{
	
	char ch = name[i];
	
	  
	
	// Convert character to binary string
	
	for (int j = 7; j >= 0; j--)
	
	{
	
	temp[index++] = (ch & 1) + '0'; // Convert bit to character '0' or '1'
	
	ch >>= 1; // Shift right by 1 bit
	
	}
	
	  
	
	if (i != len - 1)
	
	{
	
	temp[index++] = ' '; // Add space separator between binary codes
	
	}
	
	}
	
	  
	
	temp[index] = '\0'; // Null-terminate the modified string
	
	  
	
	strcpy(name, temp); // Update the original string with the modified string
	
	free(temp); // Free the allocated memory
	
	}

Fungsi-fungsi di ats merupakan fungsi yang mengganti filename dan directoryname. F
`modify_filename` mengganti nama file menjadi lowercase dengan fungsi `tolower`. `modify_directoryname` mengganti nama directory menjadi `toupper` Lalu `convert_to_binary` mengganti nama file dan directory yang kurang dari sama dengan empat menjadi binary. 


### Soal d 

Setiap ingin membuka file harus masukan password dulu. 
	
	static int xmp_open(const char *path, struct fuse_file_info *fi)
	
	{
	
	(void)fi;
	
	  
	
	if (strcmp(path, "/") == 0) {
	
	path = dirpath;
	
	} else {
	
	char fpath[1000];
	
	sprintf(fpath, "%s%s", dirpath, path);
	
	if (access(fpath, F_OK) == 0) {
	
	char input_password[100];
	
	printf("Masukkan password: ");
	
	fflush(stdout);
	
	fgets(input_password, sizeof(input_password), stdin);
	
	  
	
	size_t len = strlen(input_password);
	
	if (len > 0 && input_password[len - 1] == '\n')
	
	input_password[len - 1] = '\0';
	
	  
	
	while (strcmp(input_password, password) != 0) {
	
	printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
	
	fflush(stdout);
	
	fgets(input_password, sizeof(input_password), stdin);
	
	  
	
	len = strlen(input_password);
	
	if (len > 0 && input_password[len - 1] == '\n')
	
	input_password[len - 1] = '\0';
	
	}
	
	}
	
	}
	
	  
	
	return 0;
	
	}

Fungsi ini adalah fungsi umtuk membuka file yang harus menggunakan password lebih dahulu. 
Ketika password yang dimasukan benar, maka akan membuka file. Jika salah maka akan mengeluarkan argumen "Maaf, password salah! Silahkan coba kembali.\nMasukkan password:" dan menerima argumen password lagi. 


4. Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. Bank tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya maintenance, petinggi bank ini menyewa seseorang yang mampu mengorganisir file-file yang ada di Bank Sabar Indonesia. 
Salah satu karyawan di bank tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi file-file yang ada dalam bentuk modular, yaitu membagi file yang mulanya berukuran besar menjadi file-file chunk yang berukuran kecil. Hal ini bertujuan agar saat terjadi error, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di filesystem, Bagong membuat sebuah sistem log untuk mempermudah monitoring kegiatan di filesystem yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah file log dengan ketentuan sebagai berikut. 
- Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
- Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
- Format untuk logging yaitu sebagai berikut. [LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]

Contoh:
REPORT::230419-12:29:28::RENAME::/bsi23/bagong.jpg::/bsi23/bagong.jpeg
REPORT::230419-12:29:33::CREATE::/bsi23/bagong.jpg
FLAG::230419-12:29:33::RMDIR::/bsi23

Tidak hanya itu, Bagong juga berpikir tentang beberapa hal yang berkaitan dengan ide modularisasinya sebagai berikut yang ditulis dalam modular.c.
- a. Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
- b. Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.
- c. Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.
- d. Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.

Contoh:
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

- e. Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

## Penyelesaian

### soal 4.a dan 4.e
```c
static int xmp_mkdir(const char *path, mode_t mode)
{
    printf("path mkdir %s\n", path);
    // mkdir default
    int res;
    res = mkdir(path, mode);
    if (res == -1)
        return -errno;

    // cek modular
    int return_value;
    return_value = regcomp(&regex, prefix, 0);
    return_value = regexec(&regex, path, 0, NULL, 0);
    if(return_value == 0) {
        modularisasi(path);
        printf("MKDIR %s adalah directory modular\n", path);
    }

    // log
    writeLog("MKDIR", path, "");

    return 0;
}
```
Pertama, dilakukan pembuatan directory menggunakan fungsi `mkdir`, lalu di cek apakah nama directory tersebut berawalan `module_` menggunakan algoritma `regex`. Jika benar, maka return_value = 0 dan akan memanggil fungsi `modularisasi`. Selanjutnya dilakukan pencatatan log dengan memanggil fungsi `writeLog`.

```c
static int xmp_rename(const char *old, const char *new)
{
    // rename default
    int res;
    char oldPath[1000], newPath[1000];

    sprintf(oldPath, "%s%s", dirPath, old);
    sprintf(newPath, "%s%s", dirPath, new);
    res = rename(oldPath, newPath);
    if (res == -1)
        return -errno;
    
    // cek modular
    int return_value;
    return_value = regcomp(&regex, prefix, 0);
    return_value = regexec(&regex, newPath, 0, NULL, 0);
    if(return_value == 0) {
        printf("RENAME %s adalah directory modular\n", newPath);
        modularisasi(newPath);
    }
    else{
        remodularisasi(newPath);
    }


    writeLog("RENAME", old, new);

    return 0;
}
```
Pertama, penggantian nama akan dilakukan menggunakan fungsi `rename`, lalu di cek apakah nama directory tersebut berawalan `module_` menggunakan algoritma `regex`. Jika benar, maka return_value = 0 dan akan memanggil fungsi `modularisasi`. Jika tidak, maka akan memanggil fungsi `remodularisasi`. Selanjutnya dilakukan pencatatan log dengan memanggil fungsi `writeLog`.

### soal 4.b dan 4.d
```c
void modularisasi(const char *basePath) {
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    char filename[100];
    // readdir
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {   
            strcpy(filename, dp->d_name);
            char path[300];
            sprintf(path, "%s/%s", basePath, filename);
            int dir = 0;
            dir = isDir(path);

            // if folder -> modularisasi(folder) [rekursif]
            if (dir == 0)
            {
                char childPath[200];
                sprintf(childPath, "%s/%s", basePath, filename);
                printf("modul path : %s", childPath);
                modularisasi(childPath);
            }

            // if file -> dibagi jadi file-file kecil, hapus file asli
            else 
            {   
                char split[1000];
                sprintf(split, "split -b 1024 -a 3 -d %s %s.", path, path);
                printf("split command : %s\n", split);
                system(split);
                char rm[1000];
                sprintf(rm, "rm %s", path);
                printf("rm command : %s\n", rm);
                system(rm);
            }
        }
    }
    closedir(dir); 
}
```
Fungsi `modularisasi` digunakan untuk melakukan split file menjadi bagian-bagian yang lebih kecil, dengan ukuran sebesar 1024 bytes. Menggunakan algoritma `Directory Listing`, directory akan di open dan di read. Lalu akan di cek apakah directory tersebut berupa folder atau file menggunakan fungsi `isDir`. Jika folder, maka akan melakukan pemanggilan secara rekursif. Jika file, maka akan displit menggunakan command `split -b 1024 -a 3 -d [nama_file] [nama_file].` yang berarti akan di split menjadi 1024 bytes/file dan penamaan file akan diubah dengan penambahan 3 digit angka (dimulai dari angka 000), setelah itu file asli akan dihapus menggunakan command `rm`.

![image](/uploads/01432ee4410dbb2721171d9d0fcca096/image.png)

```c
void remodularisasi(char *basePath) {
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    char filename[100];
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {   
            strcpy(filename, dp->d_name);
            char path[300];
            sprintf(path, "%s/%s", basePath, filename); 
            printf("path: %s\n", path);
            int dir = 0;
            dir = isDir(path);

            // if folder -> remodularisasi(folder) [rekursif]
            if (dir == 0)
            {
                char childPath[200];
                sprintf(childPath, "%s/%s", basePath, filename);
                remodularisasi(path);
            }

            // if file -> dijadikan satu, hapus file asli
            else 
            {   
                char mergePath[300];
                sprintf(mergePath, "%s/%s", basePath, filename); 
                char *dot = strrchr(mergePath, '.');  
                if (dot != NULL) {
                    *dot = '\0'; 
                }
                printf("merge path: %s\n", mergePath);
                char oldPath[400] = {0};
                if (strcmp(oldPath, mergePath) != 0){
                    strcpy(oldPath, mergePath);
                    char merge[1000];
                    sprintf(merge, "cat %s.* > %s", oldPath, oldPath);
                    printf("merge command : %s\n", merge);
                    system (merge);
                    char rm[1000];
                    sprintf(rm, "rm %s.*", oldPath);
                    printf("rm command : %s\n", rm);
                    system(rm);
                }
            }
        }
    }
    closedir(dir);                        
}
```
Fungsi `remodularisasi` digunakan untuk menggabungkan split file ke keadaan utuh(fixed). Menggunakan algoritma `Directory Listing`, directory akan di open dan di read. Lalu akan di cek apakah directory tersebut berupa folder atau file menggunakan fungsi `isDir`. Jika folder, maka akan melakukan pemanggilan secara rekursif. Jika file, maka akan dimerge menggunakan command `cat [nama_file].* > [nama_file]`akan dimerge dan penamaan file akan berubah menjadi sediakala, setelah itu split file akan dihapus menggunakan command `rm`.

![image](/uploads/262d3f8728ae25eb6fafba5e5ba18b8d/image.png)

### soal 4c

```c
void writeLog(char *func, const char *prev, const char *new)
{
    time_t curTime;
    struct tm *time_info;
    time(&curTime);
    time_info = localtime(&curTime);
    int year = time_info->tm_year + 1900;
    int month = time_info->tm_mon + 1;
    int day = time_info->tm_mday;
    int hour = time_info->tm_hour;
    int min = time_info->tm_min;
    int sec = time_info->tm_sec;
    FILE *logFile;
    logFile = fopen("/home/wildan/fs_module.log", "a");
    char level[10];
    char use[3] = "";
    if (strcmp(new, "") != 0)
        strcpy(use, "::");
    if (strcmp(func, "RMDIR") == 0 || strcmp(func, "UNLINK") == 0)
        strcpy(level, "FLAG");
    else
        strcpy(level, "REPORT");
    fprintf(logFile, "%s::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s%s\n", level, day, month, year, hour, min, sec, func, prev, use, new);
    fclose(logFile);
    return;
}
```
Pertama, dilakukan penyimpanan waktu saat ini pada variabel sec, min, hour, day, month, year. Lalu, fs_module.log di open, selanjutnya di cek jika operator yang dijalankan berupa `RMDIR`dan `UNLINK` maka level = FLAG, sedangkan untuk operator lainnya level = REPORT. Setelah itu, output berupa log akan di print kedalam file dengan format sesuai dengan soal.

Selain fungsi operator diatas, terdapat juga fungsi operator lain seperti dibawah ini : 
static  struct fuse_operations xmp_oper = {

	.getattr = xmp_getattr, Berfungsi untuk mendapatkan atribut (metadata) dari sebuah file
	.access = xmp_access, Berfungsi untuk memeriksa aksesibilitas (permissions) dari sebuah file
    .readlink = xmp_readlink, Berfungsi untuk membaca isi dari tautan simbolis (symbolic link)
    .readdir = xmp_readdir, Berfungsi untuk membaca isi dari sebuah direktori
    .mknod = xmp_mknod, Berfungsi untuk membuat sebuah node (file) dalam sistem file
    .symlink = xmp_symlink, Berfungsi untuk membuat sebuah tautan simbolis (symbolic link)
    .unlink = xmp_unlink, Berfungsi untuk menghapus sebuah file
    .rmdir = xmp_rmdir, Berfungsi untuk menghapus sebuah direktori kosong
    .link = xmp_link, Berfungsi untuk membuat hard link antara dua file
    .chmod = xmp_chmod, Berfungsi untuk mengubah izin akses (permissions) dari sebuah file
    .chown = xmp_chown, Berfungsi untuk mengubah pemilik (owner) dari sebuah file
    .truncate = xmp_truncate, Berfungsi untuk memangkas ukuran file sesuai ukuran yang ditentukan
    .utimens = xmp_utimens, Berfungsi untuk mengatur waktu akses dan modifikasi dari sebuah file
    .open = xmp_open, Berfungsi untuk membuka (open) file
    .read = xmp_read, Berfungsi untuk membaca data dari sebuah file
    .write = xmp_write, Berfungsi untuk menulis data ke sebuah file
    .statfs = xmp_statfs, Berfungsi untuk mendapatkan informasi tentang sistem file (file system)
    .create = xmp_create, Berfungsi untuk membuat sebuah file baru
    .release = xmp_release, Berfungsi untuk melepaskan (release) sumber daya yang digunakan oleh file yang sedang dibuka
    .fsync = xmp_fsync, Berfungsi untuk melakukan sinkronisasi data file dengan penyimpanan fisik
    .setxattr = xmp_setxattr, Berfungsi untuk mengatur extended attribute (atribut tambahan) dari sebuah file.
    .getxattr = xmp_getxattr, Berfungsi untuk mendapatkan nilai extended attribute (atribut tambahan) dari sebuah file
    .listxattr = xmp_listxattr, Berfungsi untuk mendapatkan daftar extended attribute (atribut tambahan) dari sebuah file
    .removexattr = xmp_removexattr, Berfungsi untuk menghapus extended attribute (atribut tambahan) dari sebuah file
	
};

