#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {

    pid_t pid1, pid2;
    int status;

    pid1 = fork();
    if(pid1 == 0) {
        execlp("kaggle", "kaggle", "datasets", "download", "-d","bryanb/fifa-player-stats-database", NULL);
    }

    wait(&status);
    
    pid2 = fork();
    if (pid2 == 0) {
        execlp("unzip", "unzip", "-q", "fifa-player-stats-database.zip", NULL);
    }
    else {
        wait(&status);
            if (argc < 2) {
                system("echo \"Potential \t Age \t Name \t\t\t Club \t\t\t\t Nationality \t\t Photo \" ");
                printf("------------------------------------------------------------------------------------------------------------------------\n");
                execlp("bash", "bash", "-c", "awk -F ',' '{if($3<=24 && $8>=86 && $9!=\"Manchester City\") {print $8 \"\t\t\" $3 \"\t\" $2 \"\t\t\" $9 \"\t\t\" $5 \"\t\t\" $4} }' FIFA23_official_data.csv", NULL);
            }
            else if (argc == 2) {
                printf("You are in %s folder\n", argv[1]);
                system("echo \"Potential \t Age \t Name \t\t\t Club \t\t\t\t Nationality \t\t Photo \" ");
                printf("------------------------------------------------------------------------------------------------------------------------\n");
                execlp("bash", "bash", "-c", "awk -F ',' '{if($3<=24 && $8>=86 && $9!=\"Manchester City\") {print $8 \"\t\t\" $3 \"\t\" $2 \"\t\t\" $9 \"\t\t\" $5 \"\t\t\" $4} }' FIFA23_official_data.csv", NULL);
                printf("\n");
            }
    }
}

/*
Name 2
Age 3
Club 9
Nationality 5
Potential 8
Photo 4
*/